[![build status](https://gitlab.com/problems/react_router_mdl_js/badges/master/build.svg)](https://gitlab.com/problems/react_router_mdl_js/commits/master)

# Demo project for React + React Router and GetMDL problem

Question [posted on Stackoverflow][1].

You can check this [online here][2]

## Start

 * `npm install`
 * `npm start`

Go to `localhost:3000`

## Deploy


I use [now][3] to deploy this project.
 
 * [Install now][4] globally `npm install -g now`
 * Deploy `npm run deploy`


[1]: http://stackoverflow.com/questions/43268185/use-getmdl-with-react-react-router-throw-a-domexception
[2]: https://reactroutermdljs.now.sh/
[3]: https://zeit.co/now
[4]: https://zeit.co/download#command-line