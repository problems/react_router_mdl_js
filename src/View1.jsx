import React from 'react'
import {Link} from 'react-router'
import MdlLayout from './MdlLayout'
import Drawer from './DrawerView1'

export default class extends React.Component {

    constructor(props) {
        super(props)
        this._onClickMenu = this._onClickMenu.bind(this)
        this._onDrawerReady = this._onDrawerReady.bind(this)
    }

    _onClickMenu() {
        this.drawer.open = !this.drawer.open
    }

    _onDrawerReady(drawer) {
        this.drawer = drawer
    }

    render() {
        return <div>
            <MdlLayout title="View #1" menu={<button className="mdc-button mdc-ripple-upgraded" onClick={this._onClickMenu}>
                <i style={{color: "#FFF"}} className="material-icons">menu</i>
            </button>}>
                <Link to="v2">
                    <button className="mdc-button mdc-button--dense">View #2</button>
                </Link>
            </MdlLayout>
            <Drawer onDrawerReady={this._onDrawerReady}/>
            <main className="mdc-toolbar-fixed-adjust">
                I`m view #1
                <div className="mdc-layout-grid">
                    <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-6">
                        <p>mdc-layout-grid__cell mdc-layout-grid__cell--span-6</p>
                        <div className="mdc-form-field">
                            <input type="checkbox" id="input"/>
                            <label htmlFor="input">Input Label</label>
                        </div>
                    </div>
                    <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-4">
                        <p>mdc-layout-grid__cell mdc-layout-grid__cell--span-4</p>
                        <div className="mdc-form-field mdc-form-field--align-end">
                            <input type="checkbox" id="input"/>
                            <label htmlFor="input">Input Label</label>
                        </div>
                    </div>
                    <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-2">
                        <p>mdc-layout-grid__cell mdc-layout-grid__cell--span-2</p>
                        <label htmlFor="textfield-no-js">Textfield with no JS: </label>
                        <div className="mdc-textfield">
                            <input id="textfield-no-js" className="mdc-textfield__input" type="text"
                                   placeholder="Hint text"/>
                        </div>
                    </div>
                    <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-4">
                        <label className="mdc-textfield">
                            <input type="text" className="mdc-textfield__input"/>
                            <span className="mdc-textfield__label">Hint Text</span>
                        </label>
                    </div>
                    <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-4">
                        <div className="mdc-textfield">
                            <input type="text" id="username" className="mdc-textfield__input"
                                   aria-controls="username-helptext"/>
                            <label htmlFor="username" className="mdc-textfield__label">Using help text</label>
                        </div>
                        <p id="username-helptext" className="mdc-textfield-helptext" aria-hidden="true">
                            This will be displayed on your public profile
                        </p>
                    </div>
                    <div className="mdc-layout-grid__cell mdc-layout-grid__cell--span-4">
                        <div className="mdc-textfield mdc-textfield--fullwidth">
                            <input className="mdc-textfield__input"
                                   type="text"
                                   placeholder="Full-width"
                                   aria-label="Full-Width Textfield"/>
                        </div>
                        <div className="mdc-textfield mdc-textfield--multiline mdc-textfield--fullwidth">
                            <textarea className="mdc-textfield__input"
                                      placeholder="Full-Width multiline textfield"
                                      rows="8" cols="40"
                                      aria-label="Full-Width multiline textfield"/>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    }

}


