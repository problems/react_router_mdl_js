import React from 'react'
import PropTypes from 'prop-types';
import {drawer} from 'material-components-web';
const {MDCTemporaryDrawer} = drawer;
import {findDOMNode} from 'react-dom'
import {Link} from 'react-router'

class DrawerView1 extends React.Component {

    componentDidMount() {
        const drawerEl = findDOMNode(this)
        let drawer = new MDCTemporaryDrawer(drawerEl);

        const {onDrawerReady} = this.props
        onDrawerReady(drawer)
    }

    render() {
        return <aside className="mdc-temporary-drawer mdc-typography">
            <nav className="mdc-temporary-drawer__drawer">

                <header className="mdc-temporary-drawer__header">
                    <div className="mdc-temporary-drawer__header-content">
                        Header content goes here
                    </div>
                </header>

                <nav className="mdc-temporary-drawer__content mdc-list">
                    <Link to="v1" className="mdc-list-item">
                        <i className="material-icons mdc-list-item__start-detail" aria-hidden="true">inbox</i> #V1
                    </Link>
                    <Link to="v2" className="mdc-list-item">
                        <i className="material-icons mdc-list-item__start-detail" aria-hidden="true">star</i> #V2
                    </Link>
                </nav>
            </nav>
        </aside>
    }
}

DrawerView1.propTypes = {
    onDrawerReady: PropTypes.func.isRequired
};

export default DrawerView1